#!/usr/bin/env bash

ENV=env

python3 -m venv $ENV
source env/bin/activate
pip install --upgrade pip
pip install --upgrade -r requirements.txt