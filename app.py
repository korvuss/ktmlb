import urllib.error
import collections
from sanic import Sanic
from sanic.response import html, json

from jinja2 import Environment, PackageLoader
import random
import config
import json

env = Environment(loader=PackageLoader('app', 'templates'))
app = Sanic()

# Serve files from the "public" directory to /public.
app.static('/public', './public')


# A Rugby Union team comprises of: 2 props, 1 hooker, 2 locks, 2 flankers, 1 Number Eight, 1 Scrum Half, 1 Out Half, 2 Centres, 2 Wingers, 1 Full Back

def _get_rugby_player_data(request):
    
    uninjured_players = {"prop":[],  "hooker" :[],"lock":[],"flanker":[],"number-eight":[], "scrum-half":[], "out-half":[], "centre":[], "winger":[], "full-back":[]}
    union_squads = list();
    with open(request) as json_data:
        data = json.load(json_data)
        squads = data['squads']

        for player in data['athletes']:
            #check for uninjured players and place them in their relevant position they play
            if(player['is_injured'] == False):
                uninjured_players[player['position']].append(player)
                
        #lets add players to the squads
        for squad in squads:            
            processed_squad = _process_players(uninjured_players)            
            squad.update({"players":processed_squad})            
            union_squads.append(squad)                    
    return union_squads

def _process_players(players):
    squad_plositions=["prop","prop","hooker" ,"lock","lock","flanker","flanker","number-eight","scrum-half","out-half","centre", "centre", "winger","winger","full-back"]
    squad = list()

    for position in squad_plositions:
        #check if player available for position is not list none available
        if len(players[position])> 0:
            squad.append(random.choice(players[position]))

            players[position].remove(random.choice(players[position]))
        else:
            squad.append({"name":"No Player Available", "position":position})

    return squad

@app.route("/")
async def index(request):
    template = env.get_template('index.html')
    try:
        available_player_data = _get_rugby_player_data('rugby_athletes.json')
        html_content = template.render(data=available_player_data)
    except (AttributeError, TypeError) as e:
        html_content = e

    return html( html_content)


if __name__ == "__main__":
    app.run(host=config.host, port=config.port, debug=config.debug)
