The following program is written in python3 to connect
OS: linux (ubuntu 16LTS )


####################################################################################################
# Installation Instructions:

All following instructions will be for debian basted distro:

sudo apt-get udpate
sudo apt-get install python3
sudo apt-get install python3-pip
pip3 install -r requirements.txt

####################################################################################################

# Running System Instructions:

'''
Go to folder with code
run : python3 app.py    (for running system)
go to browser url of the server port 8000 (currently set to run on port 8000)
'''


# Proudest Achievement

Recently I designed ,architected and fully developed a Vulnerablity Management System at Integrity360. This system utilised Tenables Nessus scanner 
for information about customer networks and provides both a executive high level overview as well as a detailed indepth drill down for technical staff
to remediate issues. The system provides information over time about the health and security of a clients network(s), giving the ability to add weighting to 
different servers, asset management and reporting capabilities. I personally think this to be a proud achievement for me as I managed to design a product level system
proto-type it, pitch it to management and get buyin for the system. From there I designed a system that would fulfil customer requirements and once the system was 
production ready pitched it to clients convincing several to buyin to the solution.

# Interesting Tech articicle

Recently I found two articles covering how Intel Server chip sets are susceptible to elevation of privilege exploits. Effectively allowing hackers to remotely take control of machines running their chip sets by exploiting a bug in  the Intel Active Management Technology (AMT) that comes built into all chipsets. This fascinated me as a huge number of machines in the world run off of intel based chips.

What particularly sparked my interest in these articles was how severe this exploit was as the underlying OS wont see what is happening as the AMT directly accesses the machines network hardware.

I think this article would be of interest to you as any such exploits can affect personal machines, work machines and production level machines in your infrastructure. Being aware of recent hacks, exploits and hacking related news I personally feel is every developers responsibility to better enable them to see what is out there and how to counter act it.

http://thehackernews.com/2017/05/intel-amt-vulnerability.html
http://thehackernews.com/2017/05/intel-server-chipsets.html